package com.vadim.hasdfa.teddybear.validator

import com.vadim.hasdfa.teddybear.model.auth.User
import com.vadim.hasdfa.teddybear.model.auth.repository.UsersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import java.util.regex.Pattern

/**
 * @author Vadik Raksha
 */

@Component
class UserValidator: Validator {

    @Autowired
    private lateinit var userService: UsersRepository

    override fun validate(u: Any?, errors: Errors?) {
        val user = u as User?
        if (user == null)  {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user", "User is empty!")
            return
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "This field is required")

        if (!Pattern.compile("^[a-zA-Z0-9]{4,32}$").matcher(user.username).matches())
            errors?.rejectValue("username", "Username must be between 4 and 32 characters.")

        if (user.username == "anonymousUser")
            errors?.rejectValue("username", "Username can`t be like that!")

        if (userService.findByUsername(user.username) != null)
            errors?.rejectValue("username", "Such username already exists.")

        if (!Pattern.compile("^[a-zA-Z0-9!]{5,32}$").matcher(user.password).matches())
            errors?.rejectValue("password", "Password don`t match!")
    }

    override fun supports(clazz: Class<*>?): Boolean {
        return User::class.java == clazz
    }

}