package com.vadim.hasdfa.teddybear.config

import com.mongodb.Mongo
import com.mongodb.MongoClient
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration

@Configuration
//@EnableMongoRepositories("demo.mongo.model")
class SpringMongoConfig: AbstractMongoConfiguration() {
    //ds131237.mlab.com:31237/teddy-bear -u <dbuser> -p <dbpassword>

    private val mongoHost = "ds131237.mlab.com"
    private val mongoPort = 31237
    private val mongoDB = "teddy-bear"

    private val mongoUsername = "hasdfa-admin"
    private val mongoUserPassword = "3YV-Mwt-VbA-out".toCharArray()


    @Bean
    override fun mongo(): Mongo {
        return MongoClient(ServerAddress(mongoHost, mongoPort), mutableListOf(
                MongoCredential.createCredential(mongoUsername, mongoDB, mongoUserPassword)
        ))
    }

    override fun getDatabaseName(): String? = mongoDB
}