package com.vadim.hasdfa.teddybear.config

import com.vadim.hasdfa.teddybear.model.repository.CartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

@Component
class SessionListener: HttpSessionListener {

    @Autowired
    private lateinit var cartRepository: CartRepository

    override fun sessionCreated(event: HttpSessionEvent) {
        println("sessionCreated")
    }

    override fun sessionDestroyed(event: HttpSessionEvent) {
        println("sessionDestroyed")
        val sid = event.session.id
        val cart = cartRepository.findByUserSessionID(sid)
        if (cart != null)
            cartRepository.delete(cart)
    }
}