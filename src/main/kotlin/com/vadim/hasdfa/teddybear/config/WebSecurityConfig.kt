package com.vadim.hasdfa.teddybear.config

import com.vadim.hasdfa.teddybear.service.CustomUserDetailsService
import com.vadim.hasdfa.teddybear.utils.Roles
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurityConfig: WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var userDetailsService: CustomUserDetailsService

    override fun configure(http: HttpSecurity) {
        http.run {
            csrf().run {
                disable()
            }
            sessionManagement().run {
                sessionFixation().run {
                    newSession()
                }
            }
            authorizeRequests().run {
                // Resources
                antMatchers( "/css/**", "/js/**", "/img/**", "/assets/**").run {
                    permitAll()
                }
                antMatchers("/admin/**").run {
                    hasRole(Roles.ROLE_ADMIN)
                }
                antMatchers("/**").run {
                    permitAll()
                }
                formLogin().run {
                    loginPage("/login").run {
                        permitAll()
                    }
//                    defaultSuccessUrl("/admin")
                }
                exceptionHandling().run {
                    accessDeniedPage("/access-denied")
                }
            }
        }
    }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.run {
            userDetailsService(userDetailsService).run {
                //passwordEncoder(getPasswordEncoder())
            }
        }
    }
//    private fun getPasswordEncoder(): PasswordEncoder {
//        return object : PasswordEncoder {
//            override fun encode(charSequence: CharSequence): String {
//                return charSequence.toString()
//            }
//            override fun matches(charSequence: CharSequence, s: String): Boolean {
//                return true
//            }
//        }
//    }
}