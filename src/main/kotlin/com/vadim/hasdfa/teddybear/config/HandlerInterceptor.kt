package com.vadim.hasdfa.teddybear.config

import org.springframework.stereotype.Component
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class HandlerInterceptor: HandlerInterceptorAdapter() {

    override fun preHandle(request: HttpServletRequest?, response: HttpServletResponse?, handler: Any?): Boolean {
        println("preHandler")
        response?.setHeader("Access-Control-Allow-Origin", "*")
        return super.preHandle(request, response, handler)
    }
}
