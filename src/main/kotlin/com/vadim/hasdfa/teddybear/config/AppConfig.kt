package com.vadim.hasdfa.teddybear.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Configuration
class AppConfig: WebMvcConfigurerAdapter() {
    @Autowired
    private lateinit var handler: HandlerInterceptor

    override fun addInterceptors(registry: InterceptorRegistry?) {
        super.addInterceptors(registry)
        registry?.addInterceptor(handler)
    }

}
