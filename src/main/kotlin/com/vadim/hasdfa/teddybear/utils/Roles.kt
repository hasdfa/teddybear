package com.vadim.hasdfa.teddybear.utils

object Roles {
    internal const val ROLE_USER = "USER"
    internal const val ROLE_ADMIN = "ADMIN"

    internal const val ROLE_USER_OR_ADMIN = "hasRole('$ROLE_USER') or hasRole('$ROLE_ADMIN')"
    internal const val ROLE_USER_AND_ADMIN = "hasRole('$ROLE_USER') and hasRole('$ROLE_ADMIN')"

    object VALUES {
        internal const val ROLE_ADMIN = 1L
        internal const val ROLE_USER = 2L
    }
}