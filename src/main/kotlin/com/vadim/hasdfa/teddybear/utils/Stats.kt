package com.vadim.hasdfa.teddybear.utils

class Stats {
    companion object {
        internal const val VISITORS = "USER"
        internal const val ROLE_ADMIN = "ADMIN"
        internal const val ROLE_USER_OR_ADMIN = "hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')"
        internal const val ROLE_USER_AND_ADMIN = "hasRole('ROLE_USER') and hasRole('ROLE_ADMIN')"
    }
    class VALUES {
        companion object {
            internal const val ROLE_ADMIN = 1L
            internal const val ROLE_USER = 2L
        }
    }
}