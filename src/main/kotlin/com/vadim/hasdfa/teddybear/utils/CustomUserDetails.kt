package com.vadim.hasdfa.teddybear.utils

import com.vadim.hasdfa.teddybear.model.auth.User
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class CustomUserDetails(): UserDetails {
    private var user: User? = null
    constructor(user: User?): this() {
        this.user = user
    }

//    internal val uid: String = UUID.randomUUID().toString()

    override fun getAuthorities() = user?.roles?.map({
                SimpleGrantedAuthority(it.role.name)
            })

    override fun getPassword() = user?.password
    override fun getUsername()  = user?.username

    override fun isAccountNonExpired() = true
    override fun isAccountNonLocked() = true
    override fun isCredentialsNonExpired() = true
    override fun isEnabled() = true

    override fun toString(): String {
        return "\'$username\', \'$password\', \'HERE WILL BE UUID\'"
    }
}
