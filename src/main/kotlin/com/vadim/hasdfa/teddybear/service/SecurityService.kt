package com.vadim.hasdfa.teddybear.service

import com.vadim.hasdfa.teddybear.model.auth.User
import com.vadim.hasdfa.teddybear.model.auth.repository.UsersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.WebAuthenticationDetails
import org.springframework.stereotype.Service

@Service
class SecurityService {
    @Autowired
    private lateinit var authenticationManager: AuthenticationManager
    @Autowired
    private lateinit var userDetailsService: UserDetailsService
    @Autowired
    private lateinit var usersRepository: UsersRepository

    fun findLoggedInUsername(): String? = SecurityContextHolder.getContext().authentication.name
    fun authenticationUserDetails(): WebAuthenticationDetails = SecurityContextHolder.getContext().authentication.details as WebAuthenticationDetails

    val isLoggedIn: Boolean
    get() {
        val name = findLoggedInUsername() ?: return false
        if (name == "anonymousUser") return false
        return true
    }

    fun checkBeforeLogin(user: User): Boolean {
        val userFromDb = usersRepository.findByUsername(user.username) ?: return false
        return user == userFromDb
    }

    fun checkBeforeLogin(username: String, password: String): Boolean
            = checkBeforeLogin(User(username = username, password = password))

    fun authorities(): MutableCollection<out GrantedAuthority>?
            = SecurityContextHolder.getContext().authentication.authorities

    fun autoLogin(user: User) = autoLogin(user.username, user.password)

    fun autoLogin(username: String, password: String): Boolean {
        if (checkBeforeLogin(username, password)) {
            println("Error: $username doesn`r logged")
            return false
        }
        val userDetails = userDetailsService.loadUserByUsername(username)
        val authenticationToken = UsernamePasswordAuthenticationToken(userDetails, password, userDetails.authorities)

        authenticationManager.authenticate(authenticationToken)

        return if (authenticationToken.isAuthenticated) {
            SecurityContextHolder.getContext().authentication = authenticationToken

            println("Successfully $username auto logged in")
            true
        } else {
            println("Error: $username doesn`r logged")
            false
        }
    }

    fun hasRole(role: String): Boolean {
        val hasRole = "ROLE_$role"
        authorities()?.forEach {
            if (it.authority == hasRole) {
                println("\'${findLoggedInUsername()}\' has role: $hasRole")
                return true
            }
        }
        println("\'${findLoggedInUsername()}\' has NOT role: $hasRole")
        return false
    }
}