package com.vadim.hasdfa.teddybear.service

import com.vadim.hasdfa.teddybear.model.auth.repository.UsersRepository
import com.vadim.hasdfa.teddybear.utils.CustomUserDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService: UserDetailsService {
    @Autowired
    private lateinit var usersRepository: UsersRepository

    override fun loadUserByUsername(username: String): UserDetails? {
        val user = usersRepository.findByUsername(username) ?: throw UsernameNotFoundException("User \'$username\' can not be found!")

        return CustomUserDetails(user)
    }
}