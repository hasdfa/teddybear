package com.vadim.hasdfa.teddybear.model.auth

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Role(
        @Id val id: Long,
        var name: String = "user"
): java.io.Serializable