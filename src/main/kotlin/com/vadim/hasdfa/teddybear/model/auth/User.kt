package com.vadim.hasdfa.teddybear.model.auth

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class User(
        @Id var id: Long = 0L,
        var username: String = "",
        var password: String = "",
        @DBRef var roles: List<UserRole> = listOf()
): java.io.Serializable {
    override fun equals(other: Any?): Boolean {
        return if (other is User) {
            (other.username == this.username)
                    && (other.password == this.password)
        } else false
    }

    override fun hashCode(): Int {
        var result = username.hashCode()
        result = 31 * result + password.hashCode()
        return result
    }
}