package com.vadim.hasdfa.teddybear.model.auth.repository

import com.vadim.hasdfa.teddybear.model.auth.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UsersRepository : MongoRepository<User, Long> {
    fun findByUsername(username: String): User?
    fun findById(id: String): User?
}