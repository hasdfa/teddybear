package com.vadim.hasdfa.teddybear.model.auth

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class UserRole(
        @Id val id: Long,
        val roleId: Long = 0,
        @DBRef val role: Role = Role(roleId)
): java.io.Serializable