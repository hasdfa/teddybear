package com.vadim.hasdfa.teddybear.model.statistic.repositories

import com.vadim.hasdfa.teddybear.model.statistic.Visit
import org.springframework.data.mongodb.repository.MongoRepository

interface VisitsRepository: MongoRepository<Visit, Long> {
}