package com.vadim.hasdfa.teddybear.model.statistic

import java.util.*

data class Visit(
        val uri: String,
        val `when`: Date,
        val isAuthenticated: Boolean = false
)