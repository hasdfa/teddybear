package com.vadim.hasdfa.teddybear.model.repository

import com.vadim.hasdfa.teddybear.model.Item
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component

@Component
interface ItemsRepository: MongoRepository<Item, ObjectId> {
    fun findByTitle(title: String): List<Item>
    fun findByCategory(category: String): List<Item>
    fun findByPrice(price: Int): List<Item>
    fun findById(id: String): Item?
}