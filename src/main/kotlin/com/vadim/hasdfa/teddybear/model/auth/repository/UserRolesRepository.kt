package com.vadim.hasdfa.teddybear.model.auth.repository

import com.vadim.hasdfa.teddybear.model.auth.UserRole
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRolesRepository : MongoRepository<UserRole, Long> {
    fun findByRoleId(roleId: Int): List<UserRole>
}