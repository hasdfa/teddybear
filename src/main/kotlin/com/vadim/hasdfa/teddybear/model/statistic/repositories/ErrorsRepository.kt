package com.vadim.hasdfa.teddybear.model.statistic.repositories

import com.vadim.hasdfa.teddybear.model.statistic.Error
import org.springframework.data.mongodb.repository.MongoRepository

interface ErrorsRepository : MongoRepository<Error, Long> {
}