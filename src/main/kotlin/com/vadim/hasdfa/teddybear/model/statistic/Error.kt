package com.vadim.hasdfa.teddybear.model.statistic

import org.springframework.http.HttpStatus
import java.util.*

data class Error(
        val `when`: Date,
        val uri: String,
        val status: HttpStatus
)