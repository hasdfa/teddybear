package com.vadim.hasdfa.teddybear.model

import com.vadim.hasdfa.teddybear.model.repository.ItemsRepository
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Item(
        var id: ObjectId = ObjectId.get(),
        var title: String = "",
        var category: String = "",
        var price: Double = Double.NaN,
        var image: String = ""
): java.io.Serializable {
    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Item) return false
        return other.id == this.id
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + category.hashCode()
        result = 31 * result + price.toInt()
        result = 31 * result + image.hashCode()
        return result
    }

    companion object {
        public fun parse(id: String, itemsRepository: ItemsRepository) = itemsRepository.findById(id)
        public fun parsePOST(data: String, itemsRepository: ItemsRepository): Item? {
            //id=59edb61b7dd83c081f24a57d&title=Teddy%20bear&category=Small%20Teddy%20bear&price=25&image=img%2Fteddy.jpg
            val id = data.split("&").map { it.split("=")[1] }[0]
            return itemsRepository.findById(id)
        }
        public fun parsePOST(data: String): Item {
            //id=59edb61b7dd83c081f24a57d&title=Teddy%20bear&category=Small%20Teddy%20bear&price=25&image=img%2Fteddy.jpg
            val all = data.split("&").map { it.split("=")[1] }
            val id = ObjectId(all[0])
            val title = all[1]
            val category = all[2]
            val price = all[3].toDouble()
            val image = all[4]
            return Item(id, title, category, price, image)
        }
    }
}