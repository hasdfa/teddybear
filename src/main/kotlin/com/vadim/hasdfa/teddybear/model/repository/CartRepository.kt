package com.vadim.hasdfa.teddybear.model.repository

import com.vadim.hasdfa.teddybear.model.CartItem
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component

@Component
interface CartRepository : MongoRepository<CartItem, ObjectId> {
    fun findByUserSessionID(userSessionId: String): CartItem?
    fun findByUsername(username: String?): CartItem?
    fun findById(id: String): CartItem?
}