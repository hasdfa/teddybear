package com.vadim.hasdfa.teddybear.model.auth.repository

import com.vadim.hasdfa.teddybear.model.auth.Role
import org.springframework.data.mongodb.repository.MongoRepository

interface RolesRepository : MongoRepository<Role, Long> {
    fun findById(id: Long): Role
}