package com.vadim.hasdfa.teddybear.model

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class CartItem(
        val id: ObjectId = ObjectId.get(),
        var userSessionID: String = "",
        var username: String? = null,
        var isAuthorized: Boolean = false,
        @DBRef val items: ArrayList<Item> = arrayListOf()
)