package com.vadim.hasdfa.teddybear

import com.vadim.hasdfa.teddybear.model.auth.repository.RolesRepository
import com.vadim.hasdfa.teddybear.model.auth.repository.UserRolesRepository
import com.vadim.hasdfa.teddybear.model.auth.repository.UsersRepository
import com.vadim.hasdfa.teddybear.model.repository.CartRepository
import com.vadim.hasdfa.teddybear.model.repository.ItemsRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import java.io.InputStreamReader
import kotlin.concurrent.thread

@SpringBootApplication
class TeddyBearApplication {

    @Bean
    fun commandLineRunner(cartRepository: CartRepository, usersRepository: UsersRepository, userRolesRepository: UserRolesRepository) = CommandLineRunner {
        InputStreamReader(this.javaClass.classLoader.getResourceAsStream("helloworld")).readLines().forEach {
            println("\u001B[31m\u001B[47m$it\u001B[0m")
        }

        thread(priority = Thread.MAX_PRIORITY) {
            cartRepository.findAll().forEach { it ->
                if (!it.isAuthorized) {
                    System.out.printf("delete(%s)\n", it.userSessionID)
                    cartRepository.delete(it)
                }
            }
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TeddyBearApplication::class.java, *args)
        }
    }


    private fun printDatabase(cartRepository: CartRepository, itemsRepository: ItemsRepository,
                              usersRepository: UsersRepository, userRolesRepository: UserRolesRepository, rolesRepository: RolesRepository) {
        itemsRepository.findAll().forEach {
                println("Item(ObjectId(\"${it.id}\"), \"${it.title}\", \"${it.category}\", ${it.price}, \"${it.image}\")")
        }
        println()
        rolesRepository.findAll().forEach {
            println("Role(${it.id}, \"${it.name}\")")
        }
        println()
        userRolesRepository.findAll().forEach {
            println("UserRole(${it.id}, ${it.roleId}, Role(${it.role.id}, \"${it.role.name}\"))")
        }
        println()
        usersRepository.findAll().forEach {
            val roles = it.roles.joinToString(separator = ", ") {
                "UserRole(${it.id}, ${it.roleId}, Role(${it.role.id}, \"${it.role.name}\"))"
            }
            println("User(${it.id}, \"${it.username}\", \"${it.password}\", listOf($roles))")
        }
        println()
        cartRepository.findAll()?.forEach { it ->
            if (it.isAuthorized) {
                val items = it.items.joinToString(separator = ", ") {
                    "Item(ObjectId(\"${it.id}\"))"
                }
                println("CartItem(ObjectId(\"${it.id}\"), \"${it.userSessionID}\", \"${it.username}\", ${it.isAuthorized}, arrayListOf($items))")
            }
        }
    }
}
