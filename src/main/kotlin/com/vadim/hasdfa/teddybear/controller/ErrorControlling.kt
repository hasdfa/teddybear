package com.vadim.hasdfa.teddybear.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import javax.servlet.http.HttpServletRequest

@Controller
class ErrorControlling {

    @GetMapping("/access-denied")
    fun accessDenied(model: Model, request: HttpServletRequest): String {
        model.addAttribute("header", "Access denied")
        model.addAttribute("content", "You must be Admin to access this resource")
        println("access-denied")
        return "access-denied"
    }

    private fun getErrorCode(httpRequest: HttpServletRequest): Int {
        return httpRequest
                .getAttribute("javax.servlet.error.status_code") as Int
    }

}