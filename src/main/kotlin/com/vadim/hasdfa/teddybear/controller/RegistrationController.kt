package com.vadim.hasdfa.teddybear.controller

import com.vadim.hasdfa.teddybear.model.CartItem
import com.vadim.hasdfa.teddybear.model.auth.User
import com.vadim.hasdfa.teddybear.model.auth.UserRole
import com.vadim.hasdfa.teddybear.model.auth.repository.UserRolesRepository
import com.vadim.hasdfa.teddybear.model.auth.repository.UsersRepository
import com.vadim.hasdfa.teddybear.model.repository.CartRepository
import com.vadim.hasdfa.teddybear.service.SecurityService
import com.vadim.hasdfa.teddybear.utils.Roles
import com.vadim.hasdfa.teddybear.validator.UserValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import javax.servlet.http.HttpServletRequest

@Controller
class RegistrationController {
    @Autowired
    private lateinit var cartRepository: CartRepository

    @Autowired
    private lateinit var usersRepository: UsersRepository
    @Autowired
    private lateinit var userRolesRepository: UserRolesRepository

    @Autowired
    private lateinit var securityService: SecurityService
    @Autowired
    private lateinit var validator: UserValidator


    @GetMapping("/registration")
    fun registrationGetHandler(model: Model): String{
        if (securityService.isLoggedIn) return "redirect:/admin"
        model.addAttribute("user", User())
        return "auth/registration"
    }

    @PostMapping("/registration")
    fun registrationPostHandler(@ModelAttribute user: User,
                                model: Model,
                                request: HttpServletRequest,
                                bindingResult: BindingResult
    ): String{
        validator.validate(user, bindingResult)
        if (bindingResult.hasErrors()) {
            return "redirect:/registration?error"
        }
        val userRoleID = userRolesRepository.count()
        userRolesRepository.save(UserRole(userRoleID, Roles.VALUES.ROLE_USER))

        user.id = usersRepository.count()
        user.roles = listOf(UserRole(userRoleID))
        usersRepository.save(user)

        val cartItem = getCartItem(request.session.id)

        request.login(user.username, user.password)
        if (securityService.isLoggedIn) {
            cartItem?.let {
                it.isAuthorized = true
                it.username = securityService.findLoggedInUsername()
                cartRepository.save(it)
            }
            return if (securityService.hasRole(Roles.ROLE_ADMIN)) {
                "redirect:/admin"
            } else {
                "redirect:/"
            }
        }
        return "redirect:/registration?error"
    }


    private fun getCartItem(sid: String): CartItem? = if (securityService.isLoggedIn) {
        cartRepository.findByUsername(securityService.findLoggedInUsername()) ?: cartRepository.findByUserSessionID(sid)
    } else {
        cartRepository.findByUserSessionID(sid)
    }

    private fun getCartItems(sid: String) = getCartItem(sid)?.items ?: ArrayList()
}