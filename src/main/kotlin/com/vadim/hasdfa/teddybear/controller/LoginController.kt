package com.vadim.hasdfa.teddybear.controller

import com.vadim.hasdfa.teddybear.model.auth.User
import com.vadim.hasdfa.teddybear.service.SecurityService
import com.vadim.hasdfa.teddybear.utils.Roles
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class LoginController {

    @Autowired
    private lateinit var securityService: SecurityService

    @GetMapping("/get/{id}")
    fun getByIdHandle(@PathVariable("id") id: String, resp: HttpServletResponse) {
        if (Integer.parseInt(id) == 1)
            resp.sendRedirect("/")
        else
            try {
                resp.writer.println(id)
            } catch (e: IOException) {
                e.printStackTrace()
            }
    }

//    @GetMapping("/login/hasdfa")
//    @ResponseBody
//    fun hasdfa(request: HttpServletRequest, resp: HttpServletResponse) {
//        request.login("hasdfa", "hasdfa")
//        resp.sendRedirect("/")
//    }

    @GetMapping("/login")
    fun loginGetHandler(model: Model, request: HttpServletRequest): String {
        if (securityService.isLoggedIn) {
            return if (securityService.hasRole(Roles.ROLE_ADMIN)) {
                "redirect:/admin"
            } else {
                "redirect:/"
            }
        }
        if (checkIfCookiesEnabled(request)) return "redirect:/cookies404"

        model.addAttribute("user", User(-1L))
        return "auth/login"
    }

    @PostMapping("/loginPOST")
    fun loginPostHandler(@ModelAttribute user: User, request: HttpServletRequest): String {
        if (securityService.checkBeforeLogin(user)){
            request.login(user.username, user.password)
            if (securityService.isLoggedIn) {
                return if (securityService.hasRole(Roles.ROLE_ADMIN)) {
                    "redirect:/admin"
                } else {
                    "redirect:/"
                }
            }
        }
        return "redirect:/login?error"
    }

    @GetMapping("/role")
    fun getRole(model: Model): String{
        model.addAttribute("dataList", securityService.authorities())
        return "empty"
    }


    companion object {
        fun checkIfCookiesEnabled(request: HttpServletRequest): Boolean{
            if (request.cookies == null) return true
            return false
        }
    }
}