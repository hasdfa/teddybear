package com.vadim.hasdfa.teddybear.controller

import com.vadim.hasdfa.teddybear.model.CartItem
import com.vadim.hasdfa.teddybear.model.Item
import com.vadim.hasdfa.teddybear.model.repository.CartRepository
import com.vadim.hasdfa.teddybear.model.repository.ItemsRepository
import com.vadim.hasdfa.teddybear.service.SecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class MainController {
    @Autowired
    private lateinit var itemsRepository: ItemsRepository
    @Autowired
    private lateinit var cartRepository: CartRepository
    @Autowired
    private lateinit var securityService: SecurityService

    @GetMapping("/")
    fun index(model: Model, response: HttpServletResponse, request: HttpServletRequest): String {
        model.addAttribute("allBears", itemsRepository.findAll())

        defaultWith(model, request)
        model.addAttribute("cartBears", getCartItems(request.session.id))
        return "index"
    }

    @GetMapping("/bear/{id}")
    fun showItem(@PathVariable("id") id: String, model: Model, request: HttpServletRequest): String{
        val bear = itemsRepository.findById(id) ?: return "redirect:/cart"
        model.addAttribute("bear", bear)
        defaultWith(model, request)
        return "good"
    }

    @GetMapping("/remove/{id}")
    fun removeItemFromCart(@PathVariable("id") id: String, model: Model, request: HttpServletRequest): String{
        val item = Item.parse(id, itemsRepository) ?: return "error"
        val bear = getCartItem(request.session.id) ?: return "redirect:/cart"
        bear.items.remove(item)
        cartRepository.save(bear)
        return "redirect:/cart"
    }

    @GetMapping("/cart")
    fun cart(model: Model, request: HttpServletRequest): String {
        defaultWith(model, request)
        model.addAttribute("cartBears", getCartItems(request.session.id))
        return "cart"
    }

    @GetMapping("/about")
    fun about(model: Model, request: HttpServletRequest): String {
        defaultWith(model, request)
        return "about"
    }

    @GetMapping("/contacts")
    fun contacts(model: Model, request: HttpServletRequest): String {
        defaultWith(model, request)
        return "contacts"
    }

    @PostMapping("/add2Cart")
    @ResponseBody
    fun addToCart(@RequestBody bear: String, request: HttpServletRequest, response: HttpServletResponse): String {
        if (LoginController.checkIfCookiesEnabled(request)) {
            response.sendRedirect("/cookies404")
            return "error"
        }
        val item = Item.parsePOST(bear, itemsRepository) ?: return "error"
        val uid = request.session.id
        val inCart = getCartItem(request.session.id)
        return if (inCart == null) {
            cartRepository.save(
                    CartItem(
                            userSessionID = uid,
                            items = arrayListOf(item),
                            isAuthorized = securityService.isLoggedIn,
                            username = securityService.findLoggedInUsername()
            ))
            "added"
        } else if (!inCart.items.contains(item)) {
            inCart.items.add(item)
            cartRepository.save(inCart) != inCart
            "added"
        } else {
            inCart.items.remove(item)
            cartRepository.save(inCart) != inCart
            "removed"
        }
    }

    @GetMapping("/cookies404")
    fun cookies404() = "cookies404"

    private fun defaultWith(model: Model, request: HttpServletRequest){
        model.addAttribute("username", if(securityService.isLoggedIn) securityService.findLoggedInUsername() else null)
        val size = getCartItems(request.session.id).size
        model.addAttribute("cartSize", if(size==0) null else size)
    }

    private fun getCartItem(sid: String): CartItem? = if (securityService.isLoggedIn) {
        cartRepository.findByUsername(securityService.findLoggedInUsername()) ?: cartRepository.findByUserSessionID(sid)
    } else {
        cartRepository.findByUserSessionID(sid)
    }

    private fun getCartItems(sid: String) = getCartItem(sid)?.items ?: ArrayList()
}