package com.vadim.hasdfa.teddybear.controller

import com.vadim.hasdfa.teddybear.model.Item
import com.vadim.hasdfa.teddybear.model.repository.ItemsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class AdminPanelController {
    @Autowired
    private lateinit var itemsRepository: ItemsRepository

    @GetMapping("/admin")
    fun adminDashboard(model: Model, response: HttpServletResponse): String {
        return "admin/dashboard"
    }

    @GetMapping("/admin/profile")
    fun adminProfile(model: Model, response: HttpServletResponse, request: HttpServletRequest): String {
        return "admin/user"
    }

    @GetMapping("/admin/manage")
    fun adminManage(model: Model, response: HttpServletResponse): String {
        model.addAttribute("bears", itemsRepository.findAll())
        return "admin/table"
    }


    @GetMapping("/admin/manage/add")
    fun adminManageAddGET(model: Model, response: HttpServletResponse, request: HttpServletRequest): String {
        println(AdminPanelController.getHost(request))
        model.addAttribute("bear", Item(title = "Teddy bear", category = "Small Teddy bear", price = 0.00, image = "${AdminPanelController.getHost(request)}/img/teddy.jpg"))
        model.addAttribute("isCreated", false)
        return "admin/add"
    }

    @PostMapping("/admin/manage/add")
    fun adminManageAddPOST(@ModelAttribute bear: Item): String {
        itemsRepository.save(bear)
        return "redirect:/admin/manage"
    }

    @GetMapping("/admin/manage/add/{id}")
    fun adminManageEditGET(@PathVariable id: String, model: Model, response: HttpServletResponse, request: HttpServletRequest): String {
        val item = itemsRepository.findById(id) ?: return "redirect:/admin/manage"
        println("change/id: $item")
        model.addAttribute("bear", item)
        model.addAttribute("isCreated", true)
        return "admin/add"
    }


    @GetMapping("/admin/manage/remove/{id}")
    fun removeItemFromCart(@PathVariable("id") id: String): String{
        val bear = itemsRepository.findById(id) ?: return "redirect:/admin/manage"
        println("Remove: $bear")
        itemsRepository.delete(bear)
        return "redirect:/admin/manage"
    }


    @GetMapping("/admin/upgrade")
    fun adminUpgrade(model: Model, response: HttpServletResponse): String {
        return "admin/upgrade"
    }

    companion object {
        //public fun getHost(requestURL: String) = "${requestURL.split("/")[0]}/${requestURL.split("/")[1]}/${requestURL.split("/")[2]}"
        public fun getHost(request: HttpServletRequest) = request.requestURL.replace(request.requestURI.toRegex(), "")
    }
}