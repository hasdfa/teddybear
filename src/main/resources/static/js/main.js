document.addEventListener("DOMContentLoaded", ready);
var loader = $(".loader");

function ready() {
    window.scrollTo(0, 0);
    loader.fadeOut(100);
}

$(document).ready(function () {
    loader = $(".loader");
    $(".preloader").fadeOut(0);

    //Имитация загрузки
    $(".llogo").fadeIn(200);

    // Если поинты будут некорректно отображаться, то раскоментить
    // $("figure").animate({"left": "-100%"}, 10);
    // $("figure").animate({"left": "0%"}, 10);

    setInterval(function () {
        setInterval(function () {
            $(".llogo").fadeOut(400);
            setTimeout(function () {
                $(".llogo").fadeIn(400);
            }, 100);
        }, 600);
    }, 700);

    //Отсрочка выполнения анимации
    setTimeout(function () {
        loader.fadeOut(0);
        var slide = 0;
        $("figure").animate({"left": "0%"}, 0);
        $(".point").click(function () {
            var href = $(this).attr("href");
            $(".points").find(".point_active").remove();
            $(this).prepend("<div class='point_active'></div>");
            slide = href;
            $("figure").animate({"left": "-" + slide + "%"}, 500);
        });

        $("#slide1").click(function () {
            // var win = window.open("http://www.fisher-price.com/en_CA/brands/smarttoy/index.html", '_blank');
            // win.focus();
        });
        $("#slide2").click(function () {
            // var win = window.open("https://www.youtube.com/watch?v=_mMy1ssmTL0", '_blank');
            // win.focus();
        });
        $("#slide3").click(function () {
            // var win = window.open("https://www.google.com.ua/imgres?imgurl=http://baby-journal.eu/wp-content/uploads/2014/01/Baby-and-teddy-bear-photo_1920x1200.jpg&imgrefurl=http://baby-journal.eu/2014/01/drug-detstva-ili-affektivnaya-privyazannost-myagkaya-igrushka-dudu-2/&h=1200&w=1920&tbnid=eaFLltgWIIrLqM&tbnh=177&tbnw=284&usg=__xBwPGdm6Ogwy1Ko1CEsLMQZywr4=&hl=ru-UA&docid=8-PXkO9h8a87vM", '_blank');
            // win.focus();
        });
        setInterval(function () {
            $(".points").find(".point_active").remove();
            if (slide === 200) {
                $(".point").first().prepend("<div class='point_active'></div>");
                $("figure").animate({"left": "0%"}, 500);
                slide = 0
            } else {
                slide = parseInt(slide) + 100;
                $("[href =  " + slide +"]").prepend("<div class='point_active'></div>");
                $("figure").animate({"left": "-" + slide + "%"}, 500);
            }
        }, 7500);
        loader.remove();
    }, 1111);
});